//
//  ViewController.swift
//  CustomPomodoro
//
//  Created by Plate on 22.11.2023.
//

import UIKit

class HomeViewController: UIViewController {

    //MARK: LifeCycle
    
    override func loadView() {
        super.loadView()
        

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        let clock = UIView()
        clock.backgroundColor = .black
        clock.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(clock) // Adding the clock view to the view hierarchy

        // Setting up constraints using NSLayoutConstraint.activate
        NSLayoutConstraint.activate([
            clock.heightAnchor.constraint(equalToConstant: 50),
            clock.widthAnchor.constraint(equalToConstant: 100),
            clock.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 32),
            clock.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
        
        view.layoutIfNeeded()
        
        clock.layer.cornerRadius = clock.bounds.height / 2
    }
    
    //MARK: Setup Functions
    
    func setupUI() {
        view.backgroundColor = .red
        // Do any additional setup after loading the view.
    }
    


}

